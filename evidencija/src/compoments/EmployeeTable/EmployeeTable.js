import React, { Component } from "react";
import "./table.css";
import Table from "react-bootstrap/Table";
import ExcelButton from "../ExportExcel/genExcelButton";
import axios from "axios";
import Card from "react-bootstrap/Card";
import Spinner from "react-bootstrap/Spinner";
class App extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      employees: [],
      loading: true,
      error: false,
    };
  }

  componentDidMount() {
    axios
      .get(
        "https://script.google.com/macros/s/AKfycbwnj1Y7UGzy61tfUDuD3ZoHZBPTsTuBr0Z5nIqiPLrjIahXA3Q/exec"
      )
      .then((response) => {
        this.setState({ loading: false });
        const data = response.data[0].data;
        const employees = response.data[0].employees;
        const startWork = data.shift();
        console.log(startWork);

        employees.shift();
        this.setState({ data: data });
        const array = [];
        employees.map((e, i) => {
          let objekt = {
            id: "",
            firstName: "",
          };
          objekt.id = i;
          objekt.firstName = e;

          array.push(objekt);
        });
        this.setState({ employees: array });
      })

      .catch((error) => {
        this.setState({ loading: false });
        this.setState({ error: true });
        const errorMsg = error.message;
      });
  }

  dohvatiEmployeea(employee) {
    console.log(employee);
  }

  render() {
    console.log(this.state.employees);
    return (
      <div className="container">
        <h2 className="title">Evidencija zaposlenika</h2>
        <Card className="card">
          {this.state.loading && !this.state.error && (
            <div className="position">
              <Spinner
                size="large"
                animation="border"
                role="status"
                className="spinner"
              ></Spinner>
              <h3>Loading data</h3>
            </div>
          )}
          <Card.Body className="card-body">
            <Table className="table">
              <thead className="thead-dark">
                <tr>
                  <th>Ime i prezime</th>
                  <th>Generiraj tablicu</th>
                </tr>
              </thead>
              <tbody>
                {this.state.employees.map((employee) => {
                  return (
                    <tr key={employee.id}>
                      <th>{employee.firstName}</th>
                      <th>
                        <ExcelButton
                          data={this.state.data}
                          clickedEmployee={employee.firstName}
                        />
                      </th>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default App;
